//
//  TutorialViewController.m
//  Hackathon
//
//  Created by Michal Januszewski on 02.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import "TutorialViewController.h"
#import "TutorialPageViewController.h"

@interface TutorialViewController ()

@property (nonatomic, assign) int currentPage;
@property (nonatomic, strong) NSArray *titles;

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataSource = self;
    self.delegate = self;
    
    self.titles = @[@"Możliwy krach na rosyjskiej giełdzie.", @"Bruksela decyduje o sankcjach wobec Rosji", @"Mecz Real Madrid - FC Barcelona?", @"DDDDDDD?", @"EEEEEEEEE?"];
    
    TutorialPageViewController *tutorialPage = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialPageViewController"];
    tutorialPage.pageController = self;
    tutorialPage.titleText = @"Możliwy krach na rosyjskiej giełdzie.";
    
    [self setViewControllers:@[tutorialPage] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        NSLog(@"");
    }];
}

- (void)nextPage {
    if (self.currentPage < 2) {
        self.currentPage++;
        
        TutorialPageViewController *page = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialPageViewController"];
        page.titleText = self.titles[self.currentPage];
        page.pageController = self;
        
        [self setViewControllers:@[page]
                       direction:UIPageViewControllerNavigationDirectionForward
                        animated:YES
                      completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
