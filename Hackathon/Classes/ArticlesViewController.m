//
//  ArticlesViewController.m
//  Hackathon
//
//  Created by Michal Januszewski on 01.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import "ArticlesViewController.h"
#import "ArticleCell.h"
#import "Animator.h"
#import "TutorialPageViewController.h"
#import "ArticleTimelineViewController.h"

#import <APParallaxHeader/UIScrollView+APParallaxHeader.h>
#import <ZGParallelView/UITableView+ZGParallelView.h>
#import <ICETutorialController.h>
#import <ICETutorialPage.h>

@interface ArticlesViewController ()

@property (nonatomic, strong) NSArray *articles;
@property (nonatomic, strong) Animator *animator;

@end

@implementation ArticlesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.delegate = self;
    
    self.view.backgroundColor = [UIColor clearColor];
    self.articles = @[];
    self.animator = [Animator new];
    
    [self initHeader];
    
    [[AFJSONRequestOperation JSONRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://articles"]] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if (JSON) {
            self.articles = (NSArray*) JSON;
            [self.tableView reloadData];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"");
    }] start];
}

- (void)initHeader {
    [self.tableView addParallaxWithImage:[UIImage imageNamed:@"640x700"] andHeight:200];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    UIButton *briefButton = [[UIButton alloc] initWithFrame:CGRectMake(280, 20, 40, 30)];
    briefButton.backgroundColor = [UIColor redColor];
    [briefButton setTitle:@"BR" forState:UIControlStateNormal];
    [briefButton addTarget:self action:@selector(briefAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:briefButton];
    
    UIButton *eventButton = [[UIButton alloc] initWithFrame:CGRectMake(280, 60, 40, 30)];
    eventButton.backgroundColor = [UIColor redColor];
    [eventButton setTitle:@"EV" forState:UIControlStateNormal];
    [eventButton addTarget:self action:@selector(eventAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [window addSubview:eventButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.articles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArticleCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleCell" forIndexPath:indexPath];
    [cell initWithArticle:self.articles[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *article = self.articles[indexPath.row];
    if ([article[@"context"] length] > 0) {
        [self performSegueWithIdentifier:@"showTimeline" sender:self];
    } else {
        [self performSegueWithIdentifier:@"showArticle" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showTimeline"]) {
        ArticleTimelineViewController *timelineController = [segue destinationViewController];
        timelineController.articles = [self.articles subarrayWithRange:NSMakeRange(0, 5)];
    }
    if ([segue.identifier isEqualToString:@"showEvent"]) {
        UIViewController *destController = segue.destinationViewController;
        NSLog(@"");
    }
}

#pragma mark - Navigation Controller delegates

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    return nil;
}

- (void)briefAction:(id)sender {
    [self performSegueWithIdentifier:@"showTutorial" sender:self];
}

- (void)eventAction:(id)sender {
    [self performSegueWithIdentifier:@"showEvent" sender:self];
}

@end
