//
//  PreviewViewController.h
//  Hackathon
//
//  Created by Michal Januszewski on 02.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewViewController : UIViewController<UIScrollViewDelegate, UITextViewDelegate>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end
