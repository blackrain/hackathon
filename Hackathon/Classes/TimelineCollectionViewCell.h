//
//  TimelineCollectionViewCell.h
//  Hackathon
//
//  Created by Michal Januszewski on 01.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineCollectionViewCell : UICollectionViewCell

- (void)initWithDictionary:(NSDictionary*)dictionary;

@end
