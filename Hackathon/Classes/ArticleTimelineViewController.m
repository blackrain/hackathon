//
//  ArticleTimelineViewController.m
//  Hackathon
//
//  Created by Michal Januszewski on 01.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import "ArticleTimelineViewController.h"
#import "TimelineCollectionViewCell.h"
#import "ArticleCell.h"

@interface ArticleTimelineViewController ()

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSArray *filteredArticles;

@end

@implementation ArticleTimelineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.filteredArticles = self.articles;
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Collection View delegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TimelineCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TimelineCell" forIndexPath:indexPath];
    
    if (!self.dateFormatter) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
    }
    NSString *dayOfMonth = [NSString stringWithFormat:@"%d", indexPath.row+1];
    NSString *dayOfWeek = [NSString stringWithFormat:@"%@", [self.dateFormatter shortWeekdaySymbols][indexPath.row%7]];
    
    [cell initWithDictionary:@{@"dayofmonth": dayOfMonth, @"dayofweek": dayOfWeek}];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 30;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:2];
    for (int i=0; i<2; i++) {
        [tmp addObject:self.articles[arc4random()%5]];
    }
    self.filteredArticles = [NSArray arrayWithArray:tmp];
    
    [self.tableView reloadData];
}

#pragma mark - Tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.filteredArticles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArticleCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleCell" forIndexPath:indexPath];
    [cell initWithArticle:self.filteredArticles[indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

@end
