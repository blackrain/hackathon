//
//  TutorialPageViewController.h
//  Hackathon
//
//  Created by Michal Januszewski on 02.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TutorialViewController.h"

@interface TutorialPageViewController : UIViewController

@property (nonatomic, weak) TutorialViewController *pageController;

@property (nonatomic, strong) NSString *titleText;

@end
