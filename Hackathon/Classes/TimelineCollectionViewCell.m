//
//  TimelineCollectionViewCell.m
//  Hackathon
//
//  Created by Michal Januszewski on 01.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import "TimelineCollectionViewCell.h"

@interface TimelineCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UIView *barView;
@property (nonatomic, weak) IBOutlet UILabel *dayOfWeek;
@property (nonatomic, weak) IBOutlet UILabel *dayOfMonth;

@end

@implementation TimelineCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.selectedBackgroundView.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

- (void)initWithDictionary:(NSDictionary*)dictionary {
    self.barView.backgroundColor = @[[UIColor lightGrayColor], [UIColor orangeColor]][arc4random()%2];
    
    int height = arc4random()%48;
    CGRect fr = self.barView.frame;
    fr.origin.y = 58 - height;
    fr.size.height = height;
    self.barView.frame = fr;
    
    self.dayOfWeek.text = dictionary[@"dayofweek"];
    self.dayOfMonth.text = dictionary[@"dayofmonth"];
}

@end
