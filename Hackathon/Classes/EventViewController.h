//
//  EventViewController.h
//  Hackathon
//
//  Created by Michal Januszewski on 02.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface EventViewController : UIViewController<MKMapViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
