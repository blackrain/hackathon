//
//  TutorialPageViewController.m
//  Hackathon
//
//  Created by Michal Januszewski on 02.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import "TutorialPageViewController.h"

@interface TutorialPageViewController ()

@property (nonatomic, weak) IBOutlet UILabel *slideTitle;
@property (nonatomic, weak) IBOutlet UIButton *noButton;
@property (nonatomic, weak) IBOutlet UIButton *yesButton;

@end

@implementation TutorialPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.slideTitle.text = self.titleText;
}

- (IBAction)noAction:(id)sender {
    if (self.pageController) {
        [self.pageController nextPage];
    }
}

@end
