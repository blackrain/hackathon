//
//  ArticleCell.m
//  Hackathon
//
//  Created by Michal Januszewski on 01.04.2014.
//  Copyright (c) 2014 Grupa Wirtualna Polska. All rights reserved.
//

#import "ArticleCell.h"

@implementation ArticleCell

- (void)initWithArticle:(NSDictionary *)article {
    self.title.text = article[@"title"];
    self.categoryName.text = article[@"category"];
    self.context.text = article[@"context"];
    
    self.date.text = [self stringFromTimestamp:[article[@"published"] intValue]];
    
    [[AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:article[@"thumbnail"]]] success:^(UIImage *image) {
        self.thumbnail.image = image;
    }] start];
}

- (NSString*)stringFromTimestamp:(double)timestamp {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy, HH:mm";
    return [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
}

@end
